# Ajanda Uygulaması 
Terminalde çalışması istenen bu uygulama; bir kullanıcının notları, to-do listeleri ve günlüklerini tutması ve onları şifreleri ile koruyabilmesi imkanını ona tanımalıdır. Ek olarak bir hesap sistemi eklenebilir.

* Ödevlerinizi Ödevler klasörüne isim-sınıf.c formatında yükleyin. Eğer birden fazla dosya içeriyorlarsa aynı formatta isimlendirilmiş bir klasör açın.
